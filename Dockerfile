FROM nginx:alpine
COPY dist/ServerManager/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
